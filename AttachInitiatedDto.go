package shared

type AttachInitiatedDto struct {
	UserId uint64 `json:"userId"`
	SlotId string `json:"slotId"`
	Reason uint64 `json:"reason"`
}
