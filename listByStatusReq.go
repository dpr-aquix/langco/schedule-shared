package shared

type ListByStatusReq struct {
	UserId uint64 `json:"userId"`
	Status uint8  `json:"status"`
	Reason uint64 `json:"reason"`
}
