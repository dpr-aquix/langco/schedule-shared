package shared

type NextSlotReq struct {
	UserId uint64 `json:"userId"`
	Status uint8  `json:"status"`
}

type NextListSlotReq struct {
	Ids    []uint64 `json:"ids"`
	Status uint8    `json:"status"`
}
