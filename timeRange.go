package shared

type TimeRange struct {
	StartDate *uint64 `json:"startDate,omitempty"`
	EndDate   *uint64 `json:"endDate,omitempty"`
	UserId    uint64  `json:"userId"`
	Date      *uint64 `json:"date"`
	Duration  uint8   `json:"duration"`
	Status    uint8   `json:"status"`
	Capacity  uint8   `json:"capacity"`
	Reason    uint64  `json:"reason"`
}

func (a *TimeRange) AddSlot(count uint8, size uint) uint64 {
	return *a.Date + (uint64(count) * uint64(size))
}
