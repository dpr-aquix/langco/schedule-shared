package shared

type ListReq struct {
	Initiator *uint64 `json:"initiator"`
	Initiated *uint64 `json:"initiated"`
	From      int64   `json:"from"`
}
