package shared

type DetachSlotDto struct {
	UserId uint64 `json:"userId"`
	SlotId string `json:"slotId"`
}
