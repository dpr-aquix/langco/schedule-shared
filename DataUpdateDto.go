package shared

type DataUpdateDto struct {
	Id   string         `json:"id"`
	Data map[string]any `json:"data"`
}
