package shared

const StatusFree = uint8(0)
const StatusScheduled = uint8(1)
const StatusStarted = uint8(2)
const StatusFinished = uint8(3)
const StatusCanceled = uint8(128)
const StatusBreak = uint8(255)

const RoleParticipant = "participant"
const RolePresenter = "presenter"
