package shared

type PastListReq struct {
	Offset uint64 `json:"offset"`
	Status uint8  `json:"status"`
}
