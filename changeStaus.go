package shared

type ChangeStatusReq struct {
	Id     string `json:"id"`
	Status uint8  `json:"status"`
}
