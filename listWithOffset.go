package shared

type ListWithOffset struct {
	Status uint8 `json:"status"`
	Offset int64 `json:"offset"`
}
