package shared

type GetFirstModel struct {
	Role   uint8  `json:"role"`
	UserId uint64 `json:"userId"`
	Reason uint64 `json:"reason"`
	Status uint8  `json:"status"`
}

func (s *GetFirstModel) GetRole() string {
	if s.Role == 0 {
		return RoleParticipant
	}

	return RolePresenter
}
