package shared

type SlotCreateDto struct {
	UserId   uint64 `json:"userId"`
	Duration uint8  `json:"duration"`
	Capacity uint8  `json:"capacity"`
	Reason   uint64 `json:"reason"`
	Count    int    `json:"count"`
}
