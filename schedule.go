package shared

import "go.mongodb.org/mongo-driver/bson/primitive"

type TimeSlot struct {
	ID           primitive.ObjectID `json:"id" bson:"_id"`
	Participants ParticipantsMap    `json:"participants"`
	Presenter    uint64             `json:"presenter"`
	Data         map[string]any     `json:"data"`
	Status       uint8              `json:"status"`
	Capacity     uint8              `json:"capacity"`
	Date         *uint64            `json:"date"`
	Duration     uint8              `json:"duration"`
}

type ParticipantsMap map[uint64]Participant

type Participant struct {
	ID     uint64 `json:"id"`
	Reason uint64 `json:"reason"`
}

type ScheduleListItem struct {
	User    *uint64 `json:"user"`
	MinDate *uint64 `json:"min_date"`
}

func (s *TimeSlot) IsFree() bool {
	return s.Status == StatusFree && len(s.Participants) == 0
}

func (s *TimeSlot) IsScheduled() bool {
	return s.Status == StatusScheduled && len(s.Participants) > 0 && s.Presenter != 0
}

func (s *TimeSlot) IsCanceled() bool {
	return s.Status == StatusCanceled && len(s.Participants) > 0 && s.Presenter != 0
}

func (s *TimeSlot) IsGroup() bool {
	return s.Capacity > 1
}

func (s *TimeSlot) CanAddToGroup() bool {
	return s.Capacity > uint8(len(s.Participants))
}

func (s *TimeSlot) UserExist(userId uint64) bool {
	if s.Participants == nil {
		return false
	}

	_, ok := s.Participants[userId]

	return ok
}

func (s *TimeSlot) GetSlotReason() uint64 {
	if s.Data == nil {
		return 0
	}

	if r, ok := s.Data["reason"]; ok {
		switch v := r.(type) {
		case int64:
			return uint64(v)
		case uint64:
			return v
		}

		return 0
	}

	return 0
}

func (s *TimeSlot) GetReason(userId uint64) uint64 {
	if s.Participants == nil {
		return 0
	}

	if p, ok := s.Participants[userId]; ok {
		return p.Reason
	}

	return 0
}

func (s *TimeSlot) RestoreToFreeByParticipant(participantId uint64, reason uint64) {
	s.ID = primitive.NewObjectID()

	s.Participants = ParticipantsMap{
		participantId: Participant{
			ID:     participantId,
			Reason: reason,
		},
	}
	s.Presenter = 0
	s.Status = StatusFree
	s.Date = nil
	s.Data = nil
}

func (s *TimeSlot) Detach(userId uint64) {
	delete(s.Participants, userId)

	if len(s.Participants) == 0 {
		s.Status = StatusFree
	}
}

func (s *TimeSlot) Attach(d *TimeSlot) {
	if s.Participants == nil {
		s.Participants = ParticipantsMap{}
	}

	for k, v := range d.Participants {
		s.Participants[k] = v
	}

	s.Status = StatusScheduled
	s.Duration = d.Duration
}

func (s *TimeSlot) IdStr() string {
	return s.ID.Hex()
}
