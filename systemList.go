package shared

type SystemListReq struct {
	Date   uint64 `json:"date"`
	Status uint8  `json:"status"`
}
