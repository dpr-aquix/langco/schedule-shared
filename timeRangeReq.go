package shared

type TimeRangeReq struct {
	UserId   uint64 `json:"userId"`
	Date     uint64 `json:"date"`
	Duration uint64 `json:"duration"`
}

type TimeRangeExtendedReq struct {
	UserId   uint64 `json:"userId"`
	Date     uint64 `json:"date"`
	Duration uint64 `json:"duration"`
	Status   *uint8 `json:"status"`
	Reason   uint64 `json:"reason"`
}
